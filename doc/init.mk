# Makefile including this should first set nondefault BRANCH_DOCENEACOMMON before including it
#   and should have targets init: s_docbuild and optional initcommon: s_doceneacommon
#   Typically let doc: depend on init and initcommon inside Makefile
#   Manually optionally set GLOBALTMPCLONEROOT to yes  or  to a parent dir for tmpcommon
# This init.mk file ver
# R1.00/2016-04-29
# R2.00/2017-03-30 changed linux/documentation.git and it's old branch to linux/el_releases-common.git and it's new branch

ifeq ($(VERBOSE),yes)
VERB :=
else
VERB := @
endif

#Git repositories to be cloned
REPO_DOCBUILD      := build/docbuild.git
NAME_DOCBUILD      := docbuild
REPO_DOCENEACOMMON := linux/el_releases-common.git
NAME_DOCENEACOMMON := doceneacommon

# Set nondefault BRANCH_DOCENEACOMMON in Makefile
BRANCH_DOCENEACOMMON ?= master
# Separate clones of el_releases-common.git with different branches, needed if cloned globally with risk that different distros can be at same place
ifneq ($(BRANCH_DOCENEACOMMON),master)
NAME_DOCENEACOMMON := doceneacommon_$(BRANCH_DOCENEACOMMON)
endif

GLOBALTMPCLONEROOT = ../../tmpcommon
ifeq ($(BOOK_GLOBALCLONEROOT),)
  TMPCLONEROOT := tmpcommon
else
  ifeq ($(BOOK_GLOBALCLONEROOT),yes)
    TMPCLONEROOT := $(GLOBALTMPCLONEROOT)
  else
    TMPCLONEROOT := $(BOOK_GLOBALCLONEROOT)/tmpcommon
  endif
endif
DIR_TMPCLONEROOT := $(dir $(TMPCLONEROOT))
ifeq ($(wildcard $(DIR_TMPCLONEROOT)),)
$(error Parent of selected clone root does not exist ($(DIR_TMPCLONEROOT)))
endif

.PHONY:  initbuild initcommon usageinit cleaninit
# Keep usageinit as default target here to avoid init by mistake

usageinit: docusage
	@echo 'make initbuild  Create s_docbuild and if not exists, clone docbuild.git'
	@echo 'make initcommon Create s_doceneacommon and if not exists, clone el_releases-common.git'
	@echo '  Default clone in tmpcommon'
	@echo '  BOOK_GLOBALCLONEROOT=yes  Clone in $(GLOBALTMPCLONEROOT)'
	@echo '  BOOK_GLOBALCLONEROOT=parentdir  Clone in parentdir/tmpcommon'
	@echo '  Default branch for el_releases-common.git is master'
	@echo '  BRANCH_DOCENEACOMMON=... If another branch, Makefile should set this'
	@echo 'make pullbuild   git pull in s_docbuild'
	@echo 'make pullcommon  git pull in s_doceneacommon'
	@echo 'make cleaninit   Delete all s_* symlinks and local tmpcommon/'


# MOVE these to Makefile and set also non-default BRANCH_DOCENEACOMMON in Makefile
initbuild:    s_docbuild
initcommon:   s_doceneacommon
pullbuild:
	$(VERB)if [ ! -d s_docbuild ]; then echo "ERROR: No s_docbuild exists?" ; exit 10 ; fi
	$(VERB)cd s_docbuild ; git pull
pullcommon:
	$(VERB)if [ ! -d s_doceneacommon ]; then echo "ERROR: No s_doceneacommon exists?" ; exit 10 ; fi
	$(VERB)cd s_doceneacommon ; git pull

s_docbuild: $(TMPCLONEROOT)/$(NAME_DOCBUILD)
	$(VERB)rm s_docbuild 2>/dev/null; ln -s $(TMPCLONEROOT)/$(NAME_DOCBUILD) s_docbuild

s_doceneacommon: $(TMPCLONEROOT)/$(NAME_DOCENEACOMMON)
	$(VERB)rm s_doceneacommon 2>/dev/null; ln -s $(TMPCLONEROOT)/$(NAME_DOCENEACOMMON) s_doceneacommon

$(TMPCLONEROOT)/$(NAME_DOCBUILD):
	$(VERB)if [ ! -d "$(TMPCLONEROOT)" ] ; then mkdir -p "$(TMPCLONEROOT)" ; fi
	$(VERB)if [ -d "$(TMPCLONEROOT)/$(NAME_DOCBUILD)" ] ; then \
	     echo "Already exists $(TMPCLONEROOT)/$(NAME_DOCBUILD)" ; \
	     echo "Doing git pull instead"; cd $(TMPCLONEROOT)/$(NAME_DOCBUILD) ; git pull ; \
	   else \
	     echo "Cloning $(REPO_DOCBUILD) in $(TMPCLONEROOT)" ; \
	     cd "$(TMPCLONEROOT)" ; \
	     git clone -b master git@git.enea.se:$(REPO_DOCBUILD) $(NAME_DOCBUILD) ; \
	   fi

$(TMPCLONEROOT)/$(NAME_DOCENEACOMMON):
	$(VERB)if [ ! -d "$(TMPCLONEROOT)" ] ; then mkdir -p "$(TMPCLONEROOT)" ; fi
	$(VERB)if [ -d "$(TMPCLONEROOT)/$(NAME_DOCENEACOMMON)" ] ; then \
	    echo "Already exists $(TMPCLONEROOT)/$(NAME_DOCENEACOMMON)" ; \
	    echo "Doing git pull instead"; cd $(TMPCLONEROOT)/$(NAME_DOCENEACOMMON) ; git pull ; \
	  else \
	    echo "Cloning $(REPO_DOCENEACOMMON) in $(TMPCLONEROOT) as $(NAME_DOCENEACOMMON)" ; \
	    cd "$(TMPCLONEROOT)"; \
	    git clone -b $(BRANCH_DOCENEACOMMON) git@git.enea.se:$(REPO_DOCENEACOMMON) $(NAME_DOCENEACOMMON) ; \
	  fi

cleaninit:
	@echo "Clean only local tmpcommon/ and all local temporary symlinks s_docbuild s_targets etc."
	$(VERB)rm -rf tmpcommon 2>/dev/null ; true
	$(VERB)rm s_* 2>/dev/null ; true

#!/usr/bin/python

#------------------------------------------------------------------------------
#
# This script generates an XML file with a table with issues in Jira. See the
# variable 'conditions' for query details. It is used bu the make system in
# the generation of the release notes.
#
# The result is printed to STDOUT.
#
# It is possible to override the generation. If there is a file named
# jiraissues_override.xml in the current directory, then that file will be
# printed instead. This mechanism can be used if the table needs manual
# modifications.
#
#------------------------------------------------------------------------------

from subprocess import check_output
import json, re, datetime
import time

try:
    with open("book-enea-linux-release-info/doc/jiraissues_override.xml") as f:
        print f.read(),

    exit(0)

except SystemExit:
    # Printing the override file was successful. Exception raised by
    # the exit() call in the try block.
    exit(0)

except IOError:
    # Accessing the override file failed. Assume that it does not exist
    # and proceed with normal operation.
    pass

jd = json.JSONDecoder()

def jira_query(query):
    jira_url = "http://eneaissues.enea.com"
    fields   = "key,summary"
    query    = query.replace(" ", "+")

    cmd = ["curl",
           "-s",
           "-D-",
           "-u", "rest_reader:jira123",
           "-X", "GET",
           "-H", "Content-Type: application/json",
           jira_url + "/rest/api/2/search?jql=" + query + "&fields=" + fields
       ]

    tmp = check_output(cmd).splitlines()
    tmp = jd.decode(tmp[-1])
    return tmp["issues"]

conditions = ("project=LXCR",
              "issueType=bug",
              "resolution=Unresolved",
              'affectedversion="EL7_Std_ARM"'
          )

bugs = []

time_str = time.strftime("%Y-%m-%d, %H:%M:%S (%Z)")

for issue in jira_query(" and ".join(conditions)):
    bugs.append((issue["key"], issue["fields"]["summary"]))

print '<?xml version="1.0" encoding="ISO-8859-1"?>'
print '<!DOCTYPE section PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"'
print '"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">'
print '<section id="relinfo_extracted_from_jira">'
print '  <title>Extracted from Jira</title>'
print
print '  <para>'
print '    In the table below are the issue(s) that currently affect this specific release. Use the ticket reference provided for each issue, to look up further information if needed. Extracted at %s.' % time_str
print '  </para>'
print
print '  <remark>Jira query: (%s)</remark>' % "\n    and ".join(conditions)
print
print '  <informaltable>'
print '    <tgroup cols="2">'
print '      <colspec colwidth="6*" colname="c1"/>'
print
print '      <colspec align="center" colwidth="1*" colname="c2"/>'
print
print '      <thead>'
print '        <row>'
print '          <entry align="center">Summary</entry>'
print
print '          <entry>Enea Ref</entry>'
print '        </row>'
print '      </thead>'
print
print '      <tbody>',

if bugs:
    for bug in sorted(bugs):
        print
        print '        <row>'
        print '          <entry>%s</entry>' % bug[1]
        print
        print '          <entry>%s</entry>' % bug[0]
        print '        </row>'

else:
    print '        <row>'
    print '          <entry namest="c1" nameend="c2" align="center">'
    print '            No issues found'
    print '          </entry>'
    print '        </row>'

print '      </tbody>'
print '    </tgroup>'
print '  </informaltable>'

if bugs:
    print '  <para>Number of open bugs: %d</para>' % len(bugs)

print '</section>'

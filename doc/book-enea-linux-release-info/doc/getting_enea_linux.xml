<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<chapter id="relinfo-getting-enea-linux">
  <title>Getting Enea Linux</title>

  <para>Enea Linux is available as both pre-built binary images and source
  code. Both serve a specific purpose and each have their advantages. However,
  using the pre-built binary images allows for getting up and running faster.
  Please refer to the sections below for details on how to get Enea Linux as
  pre-built binary images or source code.</para>

  <section id="relinfo-getting-pre-built-images">
    <title>Getting Pre-built Binaries</title>

    <para>Enea Linux pre-built binaries are available for download on <ulink
    url="https://portal.enea.com/login/?redirect_to=https%3A%2F%2Fportal.enea.com%2F">portal.enea.com</ulink>.
    Log in using the credentials provided by Enea for your user account. Using
    the menu, browse to the Linux section where you will now have access to
    the <emphasis role="bold">Files</emphasis> and <emphasis
    role="bold">Online Documentation</emphasis> sections.</para>

    <para>The Files section lists each Enea Linux distribution, one for each
    version and profile, as a separate download package. Clicking on the name
    of the distribution will open a new page, which presents further details
    about the content of the release and a list of downloadable archives, one
    for each hardware target included in the release. Each archive provides
    the following content:</para>

    <itemizedlist>
      <listitem>
        <para><emphasis>images</emphasis> directory &ndash; this directory
        includes the binary image files needed to boot the target with Enea
        Linux. This includes the kernel, the root file system, device tree,
        etc.</para>
      </listitem>

      <listitem>
        <para><emphasis>sdk</emphasis> directory &ndash; this directory
        includes the installer for the SDK.</para>
      </listitem>

      <listitem>
        <para><emphasis>rpm</emphasis> directory &ndash; this directory
        contains all the packages included in the distribution in deb format,
        which can be installed using the package manager.</para>
      </listitem>
    </itemizedlist>

    <para>For faster downloads, each archive is mirrored in several places,
    geographically. Choose the archive in the region closest to you.</para>

    <para>The Documentation section lists all the documents delivered with the
    release:</para>

    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Enea Linux User&rsquo;s Guide</emphasis> -
        intended for Enea Linux platform developers, who want to configure and
        build customized Linux kernel images for embedded system targets,
        using the Yocto configuration and build system.</para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">Enea Linux Open Source Report</emphasis> -
        contains the open source and license information pertaining to
        packages provided with this release of Enea Linux.</para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">Enea Linux Release Info</emphasis> -
        provides information about the current released distribution for a
        selected set of targets, along with installation requirements and
        procedures.</para>
      </listitem>
    </itemizedlist>
  </section>

  <section id="relinfo-getting-source">
    <title>Getting the Sources</title>

    <para>Enea Linux sources are available for cloning from a set of Git
    repositories on <ulink url="https://git.enea.com">git.enea.com</ulink>.
    Since Enea Linux requires multiple repositories, Google Repo tool is used
    in order to manage configurations and make the cloning step simpler. The
    Google Repo tool uses files, known as manifests, which store a list of
    tuples (repository URL, version). The Repo tool is then used to traverse
    the list of tuples in the manifest file and clone the specified versions
    of each repository. See <ulink
    url="https://code.google.com/p/git-repo/">Google Code Archive for
    git-repo</ulink>, for more info.</para>

    <section id="getting-source-code-step-one">
      <title>Get access to git.enea.com</title>

      <para>In order to get access to git.enea.com, a ssh key is required for
      Git authentication. If you don't already have such a key, follow the
      steps below to generate one:</para>

      <orderedlist>
        <listitem>
          <para>Generate the ssh key pair:</para>

          <programlisting>$ ssh-keygen -t rsa</programlisting>

          <para>When asked for a password, just press
          <command>Enter</command>. This will create two files in the
          <literal>.ssh</literal> directory in your home directory.</para>

          <programlisting>id_rsa
id_rsa.pub</programlisting>
        </listitem>

        <listitem>
          <para>Copy the public key into an authorized_keys file:</para>

          <programlisting>$ cat id_rsa.pub &gt;&gt; authorized_keys</programlisting>
        </listitem>
      </orderedlist>

      <para>Once these steps are done and you have a valid ssh key pair, send
      the public key,
      <emphasis><emphasis><filename>id_rsa.pub</filename></emphasis></emphasis>,
      via email to <email>mailto:git_support@list.enea.se</email> in order to
      get access to <ulink
      url="https://git.enea.com">git.enea.com</ulink>.</para>
    </section>

    <section id="getting-source-code-step-two">
      <title>Get Sources</title>

      <para>To use the Repo tool to download the sources for Enea Linux, do
      the following:</para>

      <orderedlist>
        <listitem condition="hidden">
          <para>Make sure that the repo tool is installed. If not, do the
          following: <remark>Below is include of ID
          "eltf-getting-repo-install-command" from
          eltf_params_updated.xml</remark></para>

          <xi:include href="eltf_params_updated.xml"
                      xmlns:xi="http://www.w3.org/2001/XInclude"
                      xpointer="element(eltf-getting-repo-install-command/1)" />
        </listitem>

        <listitem>
          <para>Define the MACHINE to the following target listed
          here:<remark>Below is the "machine_list" programlisting in
          machine_list_generated.xml created by the make system by extracting
          from the manifest</remark></para>

          <xi:include href="machine_list_generated.xml"
                      xmlns:xi="http://www.w3.org/2001/XInclude"
                      xpointer="element(machine-list/1)" />
        </listitem>

        <listitem>
          <para>Then use the repo command below:</para>

          <xi:include href="eltf_params_updated.xml"
                      xmlns:xi="http://www.w3.org/2001/XInclude"
                      xpointer="element(eltf-repo-cloning-enea-linux/1)" />
        </listitem>
      </orderedlist>

      <para>Once the source code is downloaded, the current directory will
      contain a README file with instructions on how to build the distro and
      boot the raspberry pi machine. For ease of use, these instructions are
      also copied into this document, see <xref
      linkend="relinfo-build-boot" />.</para>

      <para>It is not necessary to explicitly clone the manifest repository
      since that is done automatically by the repo tool. To see the current
      manifest, use the following command:</para>

      <programlisting>$ repo manifest</programlisting>

      <remark>The UG should be updated with instructions on how to add
      customizations. That section should also contain more info about the
      manifest: the manifest templates, using a branch instead of the tag EL6,
      etc. When this is done a reference from here should be added.</remark>
    </section>
  </section>
</chapter>

# To be included in Makefile
# NOTE: MANIFESTHASH needs to be changed to final release tag in format refs/tags/ELnnn before a release
#       The values are shown in the release info
#       The manifest is used to fetch information into the release info from the distro files
MANIFESTHASH      ?= refs/tags/Enea_Linux_8.1
MANIFESTURL       := git@git.enea.com:linux/manifests/el_manifests-standard.git
PROFILE_NAME      := Standard
